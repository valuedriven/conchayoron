'use strict';

const bcrypt = require('bcryptjs');
const bit = require('../util/base.integration.test');
const Usuario = require('../factories/usuario.factory');

beforeAll(async () => {
  bit.addFactory(Usuario);
  await bit.init();
  bit.setUrlBase('/sessions');
});

afterAll(async () => {
  await bit.afterAll();
});

describe("usuario-testes-integracao", () => {
  
  test("uso de criptografia", async () => {
    const novaPassword = '123456';
    const usuario = await Usuario.factory.create('usuario', {
      password: novaPassword,
    })    
    const compareHash = await bcrypt.compare(novaPassword, usuario.password_hash);
    expect(compareHash).toBe(true);
  })

  test("autenticar com credenciais válidas", async () => {
    const usuario = await Usuario.factory.create('usuario');    
    const response = await bit.requestPost('', { email: usuario.email, password: usuario.password });
    expect(response.status).toBe(200);
  });

  test("bloquear autenticação devido a credenciais inválidas", async () => {
    const usuario = await Usuario.factory.create('usuario');
    const response = await bit.requestPost('', { email: usuario.email, password: usuario.password+'999' });
    expect(response.status).toBe(401);
  });

  test("receber token", async () => {
    const usuario = await Usuario.factory.create('usuario');  
    const response = await bit.requestPost('', { email: usuario.email, password: usuario.password });
    expect(response.body).toHaveProperty('token');
  }); 
});
