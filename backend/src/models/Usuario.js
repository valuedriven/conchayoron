const { Model, DataTypes } = require('sequelize');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

class Usuario extends Model {
  static init(sequelize) {
    return super.init(
      {
        nome: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.VIRTUAL,
        password_hash: DataTypes.STRING,
      },
      {
        sequelize,
        freezeTableName: true,
        hooks: {
          beforeSave: async (usuario) => {
            if (usuario.password) {
              usuario.password_hash = await bcrypt.hash(usuario.password, 8);
            }
          },
        },
      }
    );
  }
}

Usuario.prototype.checkPassword = function (password) {
  return bcrypt.compare(password, this.password_hash);
};

Usuario.prototype.generateToken = function () {
  return jwt.sign({ id: this.id }, process.env.APP_SECRET);
};

module.exports = Usuario;
