const Sequelize = require('sequelize');
const { logger } = require('../util/logger');
const Produto = require('../models/Produto');
const Op = Sequelize.Op;

var mensagem;

exports.create = (req, res) => {
  console.log(req.body);
  if (!req.body.nome) {
    mensagem = 'Conteúdo não pode ser vazio!';
    res.status(400).send({
      message: mensagem,
    });
    logger.error(mensagem);
    return;
  }

  const produto = {
    nome: req.body.nome,
    unidade: req.body.unidade,
    tipoProduto: req.body.tipoProduto,
    precoUnitario: req.body.precoUnitario,
  };

  Produto.create(produto)
    .then((data) => {
      mensagem = `Produto criado com sucesso.${data}`;
      logger.info(mensagem);
      res.send(data);
    })
    .catch((err) => {
      mensagem = `Erros ocorreram ao criar um produto.${err}`;
      res.status(500).send({
        message: mensagem,
      });
      logger.error(mensagem);
    });
};

exports.findAll = (req, res) => {
  const nome = req.query.nome;
  var condition = nome ? { nome: { [Op.iLike]: `%${nome}%` } } : null;

  Produto.findAll({ where: condition })
    .then((data) => {
      mensagem = `Produtos recuperados.${data}`;
      res.send(data);
      logger.info(mensagem);
    })
    .catch((err) => {
      mensagem = `Erros ocorreram ao criar um produto.${err}`;
      res.status(500).send({
        message: mensagem,
      });
      logger.error(mensagem);
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Produto.findByPk(id)
    .then((data) => {
      mensagem = `Produto recuperado.${data}`;
      res.send(data);
      logger.info(mensagem);
    })
    .catch((err) => {
      mensagem = `Erros ocorreram ao criar um produto.${err}`;
      res.status(500).send({
        message: mensagem,
      });
      logger.error(mensagem);
    });
};

exports.update = (req, res) => {
  const id = req.params.id;

  Produto.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num === 1) {
        mensagem = `Produto atualizado com sucesso: ${id}`;
        res.send({
          message: mensagem,
        });
        logger.info(mensagem);
      } else {
        mensagem = `Não foi possível atualizar produto com id=${id}. Talvez o produto não tenha sido encontrado ou req.body está vazio!`;
        res.send({
          message: mensagem,
        });
        logger.error(mensagem);
      }
    })
    .catch((err) => {
      mensagem = `Erro ao atualizar produto com id=${id}:${err}`;
      res.status(500).send({
        message: mensagem,
      });
      logger.error(mensagem);
    });
};

exports.delete = (req, res) => {
  const id = req.params.id;

  Produto.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num === 1) {
        mensagem = `Produto deletado com sucesso: ${id}`;
        res.send({
          message: mensagem,
        });
        logger.info(mensagem);
      } else {
        mensagem = `Não foi possível deletar produto com id=${id}. Talvez o produto não tenha sido encontrado!`;
        res.send({
          message: mensagem,
        });
        logger.error(mensagem);
      }
    })
    .catch((err) => {
      mensagem = `Não foi possível deletar produto com id=${id}-${err}`;
      res.status(500).send({
        message: mensagem,
      });
      logger.error(mensagem);
    });
};

exports.deleteAll = (req, res) => {
  Produto.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      mensagem = `${nums} Produtos foram deletados com sucesso`;
      res.send({ message: mensagem });
    })
    .catch((err) => {
      mensagem = `Erros ocorreram ao deletar todos produtos: ${err}`;
      res.status(500).send({
        message: mensagem,
      });
    });
};

exports.findAllPrecoUnitarioValido = (req, res) => {
  Produto.findAll({ where: { precoUnitario: '>0' } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || 'Erros ocorreram ao recuperar produtos.',
      });
    });
};
