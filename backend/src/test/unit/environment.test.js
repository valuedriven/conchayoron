const environment = require('../../util/environment');

jest.mock('dotenv');
jest.mock('fs');

describe('Teste environment', () => {
  afterEach(() => jest.resetAllMocks());

  test('Definir valor default para NODE_ENV...', async () => {
    fs.existsSync.mockReturnValueOnce(true);
    process.env.NODE_ENV = undefined;
    environment.init();
    expect(process.env.NODE_ENV).toEqual('development');
    expect(fs.existsSync).toHaveBeenCalledTimes(1);
  });

  test('Exceção ao não encontrar arquivo...', async () => {
    fs.existsSync.mockReturnValueOnce(false);
    expect(() => {
      environment.init();
    }).toThrow(Error);
  });
});
