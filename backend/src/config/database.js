const { logger } = require('../util/logger');
const Environment = require('../util/environment');
Environment.init();

module.exports = {
  dialect: process.env.DB_DIALECT || 'postgres',
  host: process.env.DB_HOSTNAME || 'localhost',
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USERNAME || 'conchayorouser',
  password: process.env.DB_PASSWORD || 'conchayoropwd',
  database: process.env.DB_NAME || 'conchayorodb',
  storage: process.env.DB_STORAGE || 'tmp/database.sqlite',
  logging: (msg) => logger.debug(msg),
  define: {
    timestamps: true,
    // undescored: true,
  },
};
