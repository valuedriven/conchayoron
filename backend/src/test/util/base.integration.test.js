'use strict';

const request = require('supertest');
const environment = require('../../util/environment');
environment.init();
const app = require('../../app');
const database = require('../../database');
const UsuarioFactory = require('../factories/usuario.factory');

process.on('unhandledRejection', (reason) => {
  throw reason;
});

class BaseIntegrationTest {
  constructor() {
    this.app = app;
    this.database = database;
    this.urlApp = '/api/';
    this.urlLogin = '/login';
    this.request = request;
    this.factories = new Array();
  }

  async init() {
    this.token = await this.getToken(this.usuarioComCredenciais);
    this.beforeAll();
  }

  async addFactory(factory) {
    if (!(this.factories.indexOf(factory) === undefined)) {
      this.factories.push(factory);
      await factory.init(this.database);
    }
  }

  async beforeAll() {
    // await this.database.truncate();
  }

  async afterAll() {
    // await this.database.truncate();
  }
  async requestPost(recurso, payload, byPassUrlBase, complementoPayload) {
    var url;
    if (byPassUrlBase === true) {
      url = `${this.urlApp}${recurso}`;
    } else {
      url = `${this.urlApp}${this.urlBase}${recurso}`;
    }
    return await request(this.app)
      .post(url)
      .set('Authorization', `Bearer ${this.token}`)
      .send(payload, complementoPayload);
  }
  async requestPut(recurso, payload, byPassUrlBase) {
    var url;
    if (byPassUrlBase === true) {
      url = `${this.urlApp}${recurso}`;
    } else {
      url = `${this.urlApp}${this.urlBase}${recurso}`;
    }
    return await request(this.app)
      .put(url)
      .set('Authorization', `Bearer ${this.token}`)
      .send(payload);
  }
  async requestGet(recurso, byPassUrlBase) {
    var url;
    if (byPassUrlBase === true) {
      url = `${this.urlApp}${recurso}`;
    } else {
      url = `${this.urlApp}${this.urlBase}${recurso}`;
    }
    return await request(this.app).get(url).set('Authorization', `Bearer ${this.token}`);
  }

  async requestDelete(recurso, byPassUrlBase) {
    var url;
    if (byPassUrlBase === true) {
      url = `${this.urlApp}${recurso}`;
    } else {
      url = `${this.urlApp}${this.urlBase}${recurso}`;
    }
    return await request(this.app).delete(url).set('Authorization', `Bearer ${this.token}`);
  }

  async getToken(usuario) {
    const res = await this.requestPost(`/${this.urlLogin}`, usuario, true);
    var token = res.body.access_token;
    return token;
  }

  setUrlBase(url) {
    this.urlBase = url;
  }

  getOrgaoAutuadorGeral() {
    return this.orgaoAutuadorGeral;
  }

  getUsuario() {
    return this.usuario;
  }

  getMunicipio() {
    return this.municipio;
  }
}

module.exports = new BaseIntegrationTest();
