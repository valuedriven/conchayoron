const { Model, DataTypes } = require('sequelize');

class Produto extends Model {
  static init(sequelize) {
    return super.init(
      {
        nome: {
          type: DataTypes.STRING,
        },
        unidade: {
          type: DataTypes.STRING,
        },
        tipoProduto: {
          type: DataTypes.STRING,
        },
        precoUnitario: {
          type: DataTypes.DOUBLE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
  }
}

module.exports = Produto;
