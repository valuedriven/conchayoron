const { Sequelize } = require('sequelize');
var db = require('../../models/index');
const config = require('../../config/database');
const execSync = require('child_process').execSync;
var fs = require('fs');

class Database {
  constructor() {
    this.sequelize = new Sequelize(config);
    this.sequelize.models = db.sequelize.models;
  }

  async init() {
    await this.deleteSqLiteStorage();
  }

  async drop() {
    await this.sequelize.drop({ force: true, logging: console.log });
  }

  async truncate() {
    // await this.sequelize.truncate({ force: true, cascade: true, logging: console.log });
    const modelsToTruncateOrdered = [
      'NAI',
      'EditalAIT',
      'Edital',
      'NIP',
      'RetornoBanco',
      'ImportacaoTalaoEletronico',
      'TipoVeiculo',
      'RetornoInclusaoAIT',
      'MedidaAdministrativa',
      'MarcaModelo',
      'LoteImpressao',
      'ImagemAIT',
      'EspecieVeiculo',
      'CorVeiculo',
      'Contato',
      'ConfiguracaoOrgaoAutuador',
      'CategoriaVeiculo',
      'Movimentacao',
      'Documento',
      'RespostaCampoProcesso',
      'Tramitacao',
      'Processo',
      'CampoProcesso',
      'Fluxo',
      'Passo',
      'TipoProcesso',
      'Protocolo',
      'AIT',
      'MotivoInvalidacao',
      'GrupoMotivoInvalidacao',
      'BlocoAgente',
      'Bloco',
      'TipoInfracao',
      'FaixaDetran',
      'ParecerResumido',
      'Pais',
      'Lote',
      'FaixaAR',
      'Agente',
      'TipoAgente',
      'Departamento',
      'UsuarioOrgaoAutuador',
      'Usuario',
      'OrgaoAutuador',
      'Endereco',
      'Municipio',
    ];
    for (const model of modelsToTruncateOrdered) {
      await this.sequelize.models[model].destroy({ where: {} });
    }
  }

  async sync() {
    await this.sequelize.sync({ force: true, logging: console.log });
  }

  async close() {
    await this.sequelize.close();
  }

  async truncateSpecificModel(model) {
    await model.truncate({ cascade: true });
  }
}

Database.prototype.installSequelizeCli = function () {
  return new Promise((resolve, reject) => {
    try {
      execSync('npm install -g sequelize-cli', {
        stdio: 'inherit',
      });
      resolve('sequelize-cli installed');
    } catch (e) {
      reject(e);
    }
  });
};
Database.prototype.createDb = function () {
  return new Promise((resolve, reject) => {
    try {
      execSync('sequelize db:create', {
        stdio: 'inherit',
      });
      resolve('db created');
    } catch (e) {
      reject(e);
    }
  });
};

Database.prototype.migrate = function () {
  return new Promise((resolve, reject) => {
    try {
      execSync('sequelize db:migrate', {
        stdio: 'inherit',
      });
      resolve('do all migrated');
    } catch (e) {
      reject(e);
    }
  });
};

Database.prototype.seed = function () {
  return new Promise((resolve, reject) => {
    try {
      execSync('sequelize db:seed:all --seeders-path src/db/4-0/seeds/dev', {
        stdio: 'inherit',
      });
      resolve('do all seed');
    } catch (e) {
      reject(e);
    }
  });
};

Database.prototype.deleteSqLiteStorage = async function () {
  const file = process.env.DB_STORAGE;
  if (fs.existsSync(file)) {
    fs.unlink(file, function (err) {
      if (err) throw err;
      console.log(`Arquivo ${file} deletado`);
    });
  }
};

Database.prototype.createTables = async function () {
  const promises = Object.keys(this.sequelize.models).map((key) => {
    this.sequelize.models[key].sync();
  });
  await Promise.all(promises).catch();
};

Database.prototype.connect = async function () {
  try {
    await this.sequelize.authenticate();
    console.log('Conexão com banco de dados realizada com sucesso!');
  } catch (error) {
    console.log(error);
  }
};

module.exports = new Database();
