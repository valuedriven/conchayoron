'use strict';

const bf = require('../util/base.factory');
const Produto = require('../../models/Produto');

class ProdutoFactory {
  constructor(){
    this.factory = bf;
  }
}

ProdutoFactory.prototype.init = function (database) {
  this.setFactories();  
};

ProdutoFactory.prototype.setFactories = function () {
  this.factory.define('produto', Produto, {
    nome: this.factory.sequence('produto.nome', n => `produto${n}`),
    unidade: 'cx',
    tipoProduto: 'tp1',
    precoUnitario: 10.0,
  });
};

module.exports = new ProdutoFactory();
