import axios from "axios";

export default axios.create({
  baseURL: `http://${process.env.VUE_APP_HOST}:${process.env.VUE_APP_PORT}/api`,
  headers: {
    "Content-type": "application/json"
  }
});
