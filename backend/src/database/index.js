const Sequelize = require('sequelize');
const dbConfig = require('../config/database');
const { logger } = require('../util/logger');

module.exports = class Database {
  constructor() {
    logger.info('database/index.js - inicializando');
    this.connection = new Sequelize(dbConfig);
    this.setModels();
  }

  async authenticate() {
    try {
      await this.connection.authenticate();
      logger.info('Conexão estabelecida com sucesso!');
    } catch (error) {
      logger.error('Erro ao conectar ao banco de dados: ', { error: error });
    }
  }

  setModels() {
    this.models = {
      Usuario: require('../models/Usuario').init(this.connection),
      Produto: require('../models/Produto').init(this.connection),
    };

    Object.values(this.models)
      .filter((model) => typeof model.associate === 'function')
      .forEach((model) => model.associate(this.models));
  }

  getConnection() {
    return this.connection;
  }

  getModels() {
    return this.models;
  }
};
