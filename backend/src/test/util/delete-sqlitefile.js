const environment = require('../../util/environment');
environment.init();
const fs = require('fs');
const file = process.env.DB_STORAGE;

function deletestorage() {
  console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
  console.log(`SQLite file: ${file}`);
  if (fs.existsSync(file)) {
    fs.unlink(file, function (err) {
      if (err) throw err;
      console.log(`SQLite file ${file} deletado`);
    });
  }
}
deletestorage();
