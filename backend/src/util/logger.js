'use strict';
const winston = require('winston');
const WinstonCloudWatch = require('winston-cloudwatch');
const aws = require('aws-sdk');
var util = require('util');

class Logger {
  constructor() {
    this.configureFormat();
    this.createLogger();

    if (process.env.ENABLE_CONSOLE_LOG != 'false') {
      const transportConsole = new winston.transports.Console({
        handleExceptions: true,
      });
      this.logger.add(transportConsole);
    }

    if (process.env.ENABLE_FILE_LOG != 'false') {
      const logFile = `tmp/backend-app.log`;
      const transportFile = new winston.transports.File({
        filename: logFile,
        handleExceptions: true,
      });
      this.logger.add(transportFile);
    }

    if (process.env.ENABLE_CLOUDWATCH === 'true') {
      this.configureRemoteTransport();
    }
  }

  createLogger() {
    this.logger = winston.createLogger({
      levels: winston.config.syslog.levels,
      level: process.env.LOG_LEVEL || 'info',
      format: this.customFormat,
      exitOnError: false,
    });
  }

  configureTransports() {
    this.logFile = `tmp/backend-app.log`;
    this.transportConsole = new winston.transports.Console();
    this.transportFile = new winston.transports.File({
      filename: this.logFile,
    });
  }

  configureRemoteTransport() {
    aws.config.update({
      region: process.env.AWS_DEFAULT_REGION,
    });
    const cloudwatchConfig = new WinstonCloudWatch({
      cloudWatchLogs: new aws.CloudWatchLogs(),
      logGroupName: process.env.CLOUDWATCH_LOG_GROUP_NAME,
      logStreamName: process.env.CLOUDWATCH_LOG_STREAM_NAME,
      createLogGroup: true,
      createLogStream: true,
      awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
      awsSecretKey: process.env.AWS_SECRET_ACCESS_KEY,
      retentionInDays: process.env.CLOUDWATCH_RETENTION_DAYS || 30,
    });
    this.logger.add(cloudwatchConfig);
  }

  configureFormat() {
    this.configureMessage();
    this.customFormat = winston.format.combine(
      winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss',
      }),
      winston.format.splat(),
      winston.format.json(),
      this.messageText
    );
  }

  configureMessage() {
    this.messageText = winston.format.printf(({ timestamp, level, message, ...meta }) => {
      var mensagem;
      if (Object.keys(meta).length) {
        const metaTratado = util.inspect(meta);
        mensagem = `{ "timestamp": "${timestamp}", "level": "${level}", "message": "${message}", "meta": "${metaTratado}"`;
      } else {
        mensagem = `{ "timestamp": "${timestamp}", "level": "${level}", "message": "${message}"`;
      }
      return mensagem;
    });
  }
}

module.exports = new Logger();
