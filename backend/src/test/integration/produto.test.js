'use strict';

const bit = require('../util/base.integration.test');
const Produto = require('../factories/produto.factory');

beforeAll(async () => {
  bit.addFactory(Produto);
  await bit.init();
  bit.setUrlBase('/produtos');
});

afterAll(async () => {
  await bit.afterAll();
});

describe("produto-testes-integracao", () => {
    
  test("inclusao de produto", async () => {
    const produto = await Produto.factory.attrs('produto');
    const response = await bit.requestPost('', produto);
    expect(response.status).toBe(200);
  });
});
