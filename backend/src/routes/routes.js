const express = require('express');
const { logger } = require('../util/logger');
const authMiddleware = require('../middleware/auth');
const SessionController = require('../controllers/SessionController');
const UsuarioController = require('../controllers/UsuarioController.js');
const ProdutoController = require('../controllers/ProdutoController.js');

logger.info('router.js - inicializando');
const router = express.Router();

router.get('/', (req, res) => {
  return res.json({ message: 'Bem-vindo(a) ao backend do ConchayOro!' });
});

router.post('/usuarios', UsuarioController.create);
router.get('/usuarios', UsuarioController.findAll);

router.post('/sessions', SessionController.store);

router.post('/produtos', ProdutoController.create);
router.get('/produtos', ProdutoController.findAll);
router.get('/produtos/precounitariovalido', ProdutoController.findAllPrecoUnitarioValido);
router.get('/produtos/:id', ProdutoController.findOne);
router.put('/produtos/:id', ProdutoController.update);
router.delete('/produtos/:id', ProdutoController.delete);
router.delete('/produtos', ProdutoController.deleteAll);
router.use(authMiddleware);

module.exports = router;
