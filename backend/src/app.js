const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const { logger } = require('./util/logger');
const routes = require('./routes/routes');
const Database = require('./database');
const database = new Database();

class App {
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
    this.databaseconnect();
  }

  middlewares() {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(cors());
    this.express.use(
      morgan('combined', {
        stream: { write: (message) => logger.info(message) },
      })
    );
  }

  routes() {
    this.express.use('/api', routes);
  }

  databaseconnect() {
    database.authenticate();
  }
}

module.exports = new App().express;
