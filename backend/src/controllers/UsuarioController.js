const { logger } = require('../util/logger');
const Usuario = require('../models/Usuario');

module.exports = {
  async findAll(req, res) {
    try {
      const users = await Usuario.findAll();
      return res.json(users);
    } catch (error) {
      logger.error('Erro ao recuperar registros: ', error);
      return res.status(400).json({ error });
    }
  },
  async create(req, res) {
    const { name, email, password_hash } = req.body;

    try {
      const user = await Usuario.create({ name, email, password_hash });
      return res.json(user);
    } catch (error) {
      logger.error('Erro ao criar registro: ', error);
      return res.status(400).json({ error });
    }
  },
};
