'use strict';

const bf = require('../util/base.factory');
const Usuario = require('../../models/Usuario');

class UsuarioFactory {
  constructor(){
    this.factory = bf;
  }
}

UsuarioFactory.prototype.init = function (database) {
  this.setFactories();  
};

UsuarioFactory.prototype.setFactories = function () {
  this.factory.define('usuario', Usuario, {
    nome: this.factory.chance('name'),
    email: this.factory.sequence('usuario.email', n => `usuario${n}@dominio.com`),
    password: '12345',
    password_hash: '$2b$12$cMyPV7n9iSumpp/c8h7BDu6LjeEQedfjGF6f76xHJcZwq1ga1VmcS',
  });
};

module.exports = new UsuarioFactory();
