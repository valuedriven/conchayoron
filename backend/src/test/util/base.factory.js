'use strict';

const { factory } = require('factory-girl');
const moment = require('moment');

class BaseFactory {
  constructor() {
    this.factory = factory;
    this.moment = moment;
  }

  // async define(factory, model, fields) {
  //   await this.factory.define(factory, model, fields);
  // }

  // async extend(factory, model, fields, buildOptions) {
  //   await this.factory.extend(factory, model, fields, buildOptions);
  // }

  // async assoc(factory, field) {
  //   await this.factory.assoc(factory, field);
  // }

  // async create(factory, fields) {
  //   await this.factory.create(factory, fields);
  // }

  // async attrs(factory, fields) {
  //   await this.factory.attrs(factory, fields);
  // }

  // sequence(field, value) {
  //   this.factory.sequence(field, value);
  // }

  // chance(type) {
  //   this.factory.chance(type);
  // }
}

module.exports = factory;
