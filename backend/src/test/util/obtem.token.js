'use strict';

const request = require('supertest');
const app = require('../../app');
const enderecoBase = '/api/4-0';
var enderecoAutenticacao = `${enderecoBase}/autenticacao/login`;

class ObtemToken {
  async getToken(usuario) {
    const res = await request(app).post(enderecoAutenticacao).send(usuario);
    var token = res.body.access_token;
    return token;
  }
}
module.exports = new ObtemToken();
